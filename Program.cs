﻿// See https://aka.ms/new-console-template for more information
using andromeda;
using Newtonsoft.Json;
using System.Net;

Console.WriteLine("Welcome!\n");
Console.WriteLine("Console application started\n");
Menu();

static async Task Menu()
{
    Console.WriteLine
    (
        "How to proceed?\n" +
        "--------------------------\n" +
        "1 - Read File\n" +
        "2 - Consume API\n"+
        "--------------------------\n"
    );
    string goTo = Console.ReadLine();
    switch (goTo)
    {
        case "1":
            Console.WriteLine();
            ReadFile();
            break;
        case "2":
            Console.WriteLine();
            ConsumeAPI();
            break;
        default:
            Console.WriteLine("\nSorry!\nThis operation does not accept. Try again!\n");
            Menu();
            break;
    }
}

static async Task ReadFile()
{
    Console.WriteLine("Please, enter the file path. (Only .txt)");
    try
    {
        string filePath = Console.ReadLine();
        string directory = Path.GetDirectoryName(filePath);
        string name = Path.GetFileNameWithoutExtension(filePath);
        string extension = Path.GetExtension(filePath).Replace(".", "").ToUpper();
        DateTime now = DateTime.Now;
        List<string> lines = new List<string>
        {
            $"Lines                              Created in: {now}",
            "-----------------------------------------------------------",
            "",
        };
        Console.WriteLine
        (
            "\n-----------------------------\n" +
            $"Name: {name}\n" +
            $"Directory: {directory}\n" +
            $"Type: {extension}" +
            "\n-----------------------------\n"
        );
        if (extension == "TXT")
        {
            int count = 0;
            using (var reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null) 
                { 
                    count++;
                    Console.WriteLine(line);
                    lines.Add($"Line {count} | {line}");
                }
            }
            lines.Add("");
            lines.Add("-----------------------------------------------------------");
            lines.Add($"{count} rows");
            File.WriteAllLinesAsync("WriteLines.txt", lines.ToArray());
            Retry();
        }
        else
        {
            Console.WriteLine("Bad Format! Try again.\n");
            ReadFile();
        }
    }
    catch (Exception exception)
    {
        Console.WriteLine($"-- Error : {{ {exception} }}\n\nTry again.");
        ReadFile();
    }
}

static async Task ConsumeAPI()
{
    try
    {
        var url = "https://jsonplaceholder.typicode.com/users";
        int time = 5000;
        Thread.Sleep(time);
        using (var client = new WebClient())
        {
            var data = client.DownloadString(url);
            var users = JsonConvert.DeserializeObject<List<User>>(data);
            DateTime now = DateTime.Now;
            List<string> lines = new List<string>
            {
                $"Lines                              Created in: {now}",
                "-----------------------------------------------------------",
                "",
            };
            int count = 0;
            foreach (var user in users)
            {
                count++;
                Console.WriteLine(user.username);
                lines.Add($"Line {count} | {user.username}");
            }
            lines.Add("");
            lines.Add("-----------------------------------------------------------");
            lines.Add($"{count} rows");
            File.WriteAllLinesAsync("WriteLines.txt", lines.ToArray());
            Retry();
        }
    }
    catch (Exception exception)
    {
        Console.WriteLine($"-- Error : {{ {exception} }}\n\nTry again.");
        Menu();
    }
}

static async Task Retry() 
{
    Console.Write("\nReturn? (y/n): ");
    var response = Console.ReadLine();
    Console.WriteLine();
    if (response.ToLower() == "y")
    {
        Menu();
    }
}